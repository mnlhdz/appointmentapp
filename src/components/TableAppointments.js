import React from "react";
import {Button, Table} from "react-bootstrap";
import {BsTrash } from "react-icons/bs";
import {VscEdit } from "react-icons/vsc";
import {Link} from "react-router-dom";
import Moment from "react-moment";

function TableAppointments(props) {

    function renderRow(appointment) {
        return <tr className="text-center" key={appointment.id}>
            <td>{appointment.id}</td>
            <td>{appointment.doctor.name}</td>
            <td><Moment format="YYYY/MM/DD hh:mm">{appointment.date}</Moment></td>
            <td>{appointment.status}</td>
            <td><Link to={ "/appointment/" + appointment.id }><VscEdit /></Link>&nbsp;&nbsp;&nbsp;<a href="#" onClick={ ()=> props.deleteAppointment( appointment.id ) }><BsTrash /></a></td>
        </tr>;
    }

    return (
        <Table striped bordered hover>
            <thead>
            <tr  className="text-center">
                <th>#</th>
                <th>Doctor</th>
                <th>Date</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            { props.appointments.map( renderRow ) }
            </tbody>
        </Table>
    );
}

export default TableAppointments;
