import React from "react";
import {Button, Container, Form, Row} from "react-bootstrap";

function Login() {
    return (
        <div className="General-Container">
            <Container className="justify-content-md-center mb-3 col-3">
                    <Row className="justify-content-md-center">
                        <Form action="/appointments">
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Control type="text" name="username" placeholder="Usuario"/>
                            </Form.Group>
                            <Form.Group  className="mb-3" controlId="formBasicPassword">
                                <Form.Control type="password" name="password" placeholder="Contraseña"/>
                            </Form.Group>
                            <Button variant="primary" type="submit">Login</Button>
                        </Form>
                    </Row>
            </Container>
        </div>
    );
}

export default Login;