import React from "react";
import {Container, Row, Table} from "react-bootstrap";
import TableAppointments from "../components/TableAppointments";
import * as AppointmentService from "../api/appointmentService";
import {Link} from "react-router-dom";

function Appointments() {

    const [appointments, setAppointments] = React.useState([]);

    React.useEffect(() => {
        AppointmentService.getAppointments().then(appointments => {
            setAppointments(appointments.content);
        });
    }, []);

    function deleteAppointment(id) {
        AppointmentService.deleteAppointment(id).then(data => {
            console.log(data);
            let newAppointments = appointments.filter(appointment => appointment.id !== id);
            setAppointments(newAppointments);
        });
    }

    return (
        <Container className="justify-content-center mt-3" >
            <Container className="justify-content-center mb-3">
                <Row className="justify-content-center">
                    <Link className="btn btn-outline-primary" to="/appointment">Crear cita</Link>
                </Row>
            </Container>
            <TableAppointments appointments={appointments} deleteAppointment={ deleteAppointment }/>
        </Container>
    );
}

export default Appointments;