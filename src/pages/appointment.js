import React, {useEffect} from "react";
import {useParams, useNavigate } from "react-router-dom";
import {Button, Container, Form} from "react-bootstrap";
import * as UserService from "../api/userService";
import * as AppointmentService from "../api/appointmentService";

function Appointment() {
    const {id} = useParams();

    const navigate = useNavigate();

    const [appointment, setAppointment] = React.useState({
        id: null,
        date: null,
        status: "PENDING",
        doctor: {
            id: null,
        },
        patient: { id: 3 },
        day: null,
        hour: null
    });

    const [doctors, setDoctors] = React.useState([]);


    useEffect(() => {
        if(doctors.length === 0) {
            console.log("loading doctors");
            UserService.getDoctors().then(data => {
                setDoctors(data);
            });

        }else if(id) {
            console.log("loading appointment");
            AppointmentService.getAppointment(id).then(data => {
                let day = data.date.split("T")[0];
                let hour = data.date.split("T")[1].split("+")[0];
                data = {...data, date: day+"T"+hour, day: day, hour: hour, id: id};
                setAppointment(data);
            });
        }
    }, [doctors.length, id]);

    function optionDoctor(doctor) {
        return <option key={doctor.id} value={doctor.id}>{doctor.name}</option>;
    }

    function submit(event) {
        event.preventDefault();
        console.log(appointment);
        AppointmentService.saveAppointment(appointment).then(data => {
            console.log(data);
            navigate("/appointments");
        });
    }

    function handleOnChange({target}) {
        const updateAppointment= {...appointment, [target.name]: target.value};
        setAppointment( updateAppointment );
    }

    function handleOnChangeDoctor({target}) {
        let newAppointment = {...appointment, doctor: { id : target.value }};
        setAppointment( newAppointment );
    }

    function handleOnChangeDate({target}) {
        let previousDate = appointment.date;
        let newDate = target.value
        if( previousDate && previousDate.split("T").length > 1 ) {
            let appointmentDate = previousDate.split("T");
            newDate += "T" + appointmentDate[1];
        }
        let newAppointment = {...appointment, date: newDate, day: target.value};
        setAppointment( newAppointment );
    }

    function handleOnChangeTime({target}) {
        let appointmentDate = appointment.date.split("T")[0];
        let newAppointment = {...appointment, date: appointmentDate + "T" + target.value, hour: target.value};

        setAppointment( newAppointment );
    }


    return (
        <Container className="justify-content-center mt-3">
            <h1 className="text-center">{ ( appointment && appointment.id) ? "Update appointment":"New appointment" }</h1>
            <Form onSubmit={ submit }>
                <Form.Group  className="mb-3" controlId="formBasicDoctor">
                    <Form.Label>Doctor</Form.Label>
                    <Form.Select name="doctorId" value={ appointment.doctor.id } onChange={ handleOnChangeDoctor } placeholder="Enter email">
                        <option value="">Select doctor</option>
                        { doctors.map( optionDoctor ) }
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicDate">
                    <Form.Label>Date</Form.Label>
                    <Form.Control type="date" value={appointment.day} onChange={handleOnChangeDate}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicTime">
                    <Form.Label>Time</Form.Label>
                    <Form.Select value={appointment.hour} onChange={ handleOnChangeTime }>
                        <option value="">Select time</option>
                        <option value="11:00:00.000">11:00</option>
                        <option value="12:00:00.000">12:00</option>
                        <option value="13:00:00.000">13:00</option>
                        <option value="14:00:00.000">14:00</option>
                        <option value="15:00:00.000">15:00</option>
                        <option value="16:00:00.000">16:00</option>
                        <option value="17:00:00.000">17:00</option>
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicStatus">
                    <Form.Label>Status</Form.Label>
                    <Form.Select name="status" value={appointment.status} onChange={ handleOnChange }>
                        <option value="PENDING">Pending</option>
                        <option value="COMPLETED">Completed</option>
                        <option value="CANCELED">Canceled</option>
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicNotes">
                    <Form.Label>Notes</Form.Label>
                    <Form.Control as="textarea" rows="3" name="note" value={appointment.note} onChange={ handleOnChange }/>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </Container>
    );
}

export default Appointment;