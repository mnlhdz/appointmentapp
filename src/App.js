import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Login from "./pages/login";
import {Routes, Route} from 'react-router-dom';
import Appointment from "./pages/appointment";
import Appointments from "./pages/appointments";

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>My appointments</h1>
            </header>
            <Routes>
                <Route path="/" element={<Login />}/>
                <Route path="/appointments" element={<Appointments />}/>
                <Route path="/appointment" element={<Appointment />}/>
                <Route path="/appointment/:id" element={<Appointment />}/>
            </Routes>
        </div>
    );
}

export default App;
