import {handleResponse, handleError} from "./apiUtils";

//const baseUrl = process.env.APP_API_URL + "/user/";
const baseUrl = "http://localhost:8080/user/";

export function getUsers(pageNumber = 0, pageSize =10, sortBy="date", sortOrder = "desc") {
    const url = baseUrl + "?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder;
    console.log(url);
    return fetch( url )
        .then(handleResponse)
        .catch(handleError);
}

export function getUser(userId) {
    const url = baseUrl + userId;
    return fetch(url)
        .then(handleResponse)
        .catch(handleError);
}

export function saveUser(user) {
    return fetch(baseUrl + (user.id || ""), {
        method: user.id ? "PUT" : "POST", // POST for create, PUT to update when id already exists.
        headers: { "content-type": "application/json" },
        body: JSON.stringify(user)
    })
        .then(handleResponse)
        .catch(handleError);
}

export function deleteUser(userId) {
    return fetch(baseUrl + userId, { method: "DELETE" })
        .then(handleResponse)
        .catch(handleError);
}

export function getDoctors(){
    const url = baseUrl + "doctors/";
    return fetch(url)
        .then(handleResponse)
        .catch(handleError);
}
