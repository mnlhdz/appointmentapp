import { handleResponse, handleError } from "./apiUtils";
const baseUrl1 = process.env.APP_API_URL + "/appointment/";
const baseUrl = "http://localhost:8080/appointment/";

export function getAppointments(pageNumber = 0, pageSize =10, sortBy="date", sortOrder = "desc") {

    const url = baseUrl + "?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&sortBy=" + sortBy + "&sortOrder=" + sortOrder;
    console.log(url);
    return fetch( url )
        .then(handleResponse)
        .catch(handleError);
}


export function getAppointment(appointmentId) {
    const url = baseUrl + appointmentId;
    return fetch(url)
        .then(handleResponse)
        .catch(handleError);
}

export function saveAppointment(appointment) {
    return fetch(baseUrl + (appointment.id || ""), {
        method: appointment.id ? "PUT" : "POST", // POST for create, PUT to update when id already exists.
        headers: { "content-type": "application/json" },
        body: JSON.stringify(appointment)
    })
        .then(handleResponse)
        .catch(handleError);
}

export function deleteAppointment(appointmentId) {
    return fetch(baseUrl + appointmentId, { method: "DELETE" })
        .then(handleResponse)
        .catch(handleError);
}
